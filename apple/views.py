from django.shortcuts import render


def index_page(request):
    return render(request, 'index.html')


def calculator(request):
    context = {}
    context['username'] = 'Polka'
    # ?a=5&b=3
    context['a'] = request.GET.get('a', 1)
    context['b'] = request.GET.get('b', 1)
    context['sum'] = str(int(context['a']) + int(context['b']))
    return render(request, 'calculator.html', context)
